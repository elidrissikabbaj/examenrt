package exam.rt.examenrt;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class SpringBatchConfiguration {
    @Bean
    public FlatFileItemReader<MyObject> reader() {
        FlatFileItemReader<MyObject> reader = new FlatFileItemReader<>();
        reader.setResource(new ClassPathResource("votre_fichier.csv"));
        reader.setLineMapper(new DefaultLineMapper<MyObject>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames("colonne1", "colonne2");
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<MyObject>() {{
                setTargetType(MyObject.class);
            }});
        }});
        return reader;
    }

    @Bean
    public ItemProcessor<MyObject, MyObject> processor() {
        return new CustomItemProcessor();
    }

    public class CustomItemProcessor implements ItemProcessor<MyObject, MyObject> {
        @Override
        public MyObject process(MyObject item) throws Exception {
            // Implémentez la logique de détection des lignes altérées ici
            if (/* condition de détection */) {
                // Marquez la ligne comme altérée
                item.setAlt(true);
            }
            return item;
        }
    }

    @Bean
    public Job myJob(JobBuilderFactory jobBuilderFactory,
                     StepBuilderFactory stepBuilderFactory,
                     ItemReader<MyObject> reader,
                     ItemProcessor<MyObject, MyObject> processor,
                     ItemWriter<MyObject> writer) {

        Step step = stepBuilderFactory.get("step1")
                .<MyObject, MyObject>chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();

        return jobBuilderFactory.get("myJob")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

}
