package exam.rt.examenrt;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBProducer {

    public static void main(String[] args) {
        // Kafka producer configuration
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        // JDBC connection configuration
        String jdbcUrl = "jdbc:sqlserver://localhost;database=AdventureWorks2012;encrypt=true;trustServerCertificate=true;";
        String username = "sa";
        String password = "IDSI2023@ginfo";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            // Query data from the database
            String query = "SELECT * FROM Sales.SalesReason";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    // Kafka producer
                    try (Producer<String, String> producer = new KafkaProducer<>(properties)) {
                        while (resultSet.next()) {
                            // Extract data from the result set and produce to Kafka
                            System.out.println("hi"+resultSet.toString());
                            String key = resultSet.getString("SalesReasonID");
                            String value = resultSet.getString("value");
                            ProducerRecord<String, String> record = new ProducerRecord<>("examrt", key, value);
                            producer.send(record);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
