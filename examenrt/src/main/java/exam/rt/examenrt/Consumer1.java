package exam.rt.examenrt;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class Consumer1 {

    public static void main(String[] args) {
        String bootstrapServers = "localhost:9092";
        String groupId = "my_consumer_group";
        String topic = "examrt";
        String csvFilePath = "output.csv";

        // Configuration du consommateur Kafka
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Créer une instance du consommateur Kafka
        Consumer<String, String> consumer = new KafkaConsumer<>(properties);

        // S'abonner au topic
        consumer.subscribe(Collections.singletonList(topic));

        // Créer un fichier CSV
        try (FileWriter csvWriter = new FileWriter(csvFilePath)) {
            // Lire des messages du topic Kafka
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000)); // Réglage du délai en millisecondes

                records.forEach(record -> {
                    // Écrire le message dans le fichier CSV
                    try {
                        csvWriter.append(record.value()).append("\n");
                        csvWriter.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Fermer le consommateur Kafka à la fin
            consumer.close();
        }
    }
}

